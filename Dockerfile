FROM continuumio/miniconda3
LABEL authors="Anthony Underwood" \
      description="Docker image CI"
COPY environment.yml /
RUN conda env create -f /environment.yml && conda clean -a

ENV PATH /opt/conda/envs/blast/bin:$PATH