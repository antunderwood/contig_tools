from unittest import TestCase
import os
from contig_tools.file_parsing import import_contig_records
from contig_tools.filter_contigs import filtered_contigs_generator

class TestFastaFileParsing(TestCase):
    def test_fasta_file_filtering(self):
        test_fasta_path = '{0}/test_data/contigs_for_checks.fas'.format(os.path.dirname(os.path.realpath(__file__)))
        contigs = import_contig_records(test_fasta_path)
        self.assertEqual(3, len(list(filtered_contigs_generator(contigs, min_length = 10))))
        self.assertEqual(2, len(list(filtered_contigs_generator(contigs, min_length = 10, min_coverage = 5))))