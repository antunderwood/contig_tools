from unittest import TestCase
import os
from contig_tools.file_parsing import import_contig_records
from contig_tools.contig_metrics import get_contig_metrics, Nx
from contig_tools.filter_contigs import filtered_contigs_generator

class TestNx(TestCase):
    def test_n50(self):
        self.assertEqual(8, Nx([2,3,4,5,6,7,8,9,10]))

    def test_n50_unordered(self):
        self.assertEqual(8, Nx([8,9,10,2,3,4,5,6,7]))
    
    def test_n50_specifiying_x(self):
        self.assertEqual(8, Nx([8,9,10,2,3,4,5,6,7], x = 50))

    def test_n75(self):
        self.assertEqual(5, Nx([8,9,10,2,3,4,5,6,7], x = 75))

class TestFastaFileParsing(TestCase):
    def setUp(self):
        self.test_fasta_path = '{0}/test_data/contigs_for_checks.fas'.format(os.path.dirname(os.path.realpath(__file__)))

    def test_fasta_file_metrics(self):
        contigs = import_contig_records(self.test_fasta_path)
        self.assertEqual(
            {'%GC' : 50,
            'Largest contig': 20,
            'Number of contigs > 500bp': 0,
            'N50 score' : 16,
            'Number of contigs': 5, 
            'Total length': 60},
            get_contig_metrics(contigs)
        )

    def test_fasta_file_metrics_after_filtering(self):
        contigs = import_contig_records(self.test_fasta_path)
        filtered_contigs = list(filtered_contigs_generator(contigs, min_length = 10))
        self.assertEqual(
            {'%GC' : 50,
            'Largest contig': 20,
            'Number of contigs > 500bp': 0,
            'N50 score' : 16,
            'Number of contigs': 3,
            'Total length': 48},
            get_contig_metrics(filtered_contigs)
        )
