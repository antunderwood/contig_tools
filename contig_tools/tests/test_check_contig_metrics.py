from unittest import TestCase
import os
from contig_tools.file_parsing import import_contig_records
from contig_tools.filter_contigs import filtered_contigs_generator
from contig_tools.check_metrics import check_contig_metrics
from contig_tools.contig_metrics import get_contig_metrics


class TestFastaFileParsingAndChecking(TestCase):
    def setUp(self):
        self.test_fasta_path = '{0}/test_data/contigs_for_checks.fas'.format(os.path.dirname(os.path.realpath(__file__)))
        self.pass_yaml_path = '{0}/test_data/conditions_pass.yml'.format(os.path.dirname(os.path.realpath(__file__)))
        self.fail_yaml_path = '{0}/test_data/conditions_fail.yml'.format(os.path.dirname(os.path.realpath(__file__)))
        contigs = import_contig_records(self.test_fasta_path)
        filtered_contigs = list(filtered_contigs_generator(contigs, min_length = 10))
        self.contig_metrics =  get_contig_metrics(filtered_contigs)

    def test_contig_conditions_pass(self):
        result, _ = check_contig_metrics(self.contig_metrics, self.pass_yaml_path)
        self.assertEqual(True, result)

    def test_fail_contig_conditions_pass(self):
        result, falure_reasons = check_contig_metrics(self.contig_metrics, self.fail_yaml_path)
        self.assertEqual(False, result)
        self.assertEqual(['Total length: not 100 > 48 > 50'], falure_reasons)
        