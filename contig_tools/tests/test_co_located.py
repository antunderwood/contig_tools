from unittest import TestCase
import os
from contig_tools.co_located import make_blast_database
from contig_tools.co_located import blast_search
from contig_tools.co_located import parse_blast
from contig_tools.co_located import write_result_table
from contig_tools.co_located import find_co_located

import tempfile



class TestCompanionFunctions(TestCase):
    def setUp(self):
        self.genome_file_path = f'{os.path.dirname(os.path.realpath(__file__))}/test_data/genome_contigs.fas'
        self.genome_file_path2 = f'{os.path.dirname(os.path.realpath(__file__))}/test_data/genome_contigs2.fas'
        self.co_located_query_file_path = f'{os.path.dirname(os.path.realpath(__file__))}/test_data/co-located_queries.fas'
        self.non_co_located_query_file_path = f'{os.path.dirname(os.path.realpath(__file__))}/test_data/non_co-located_queries.fas'
        self.just_one_match_co_located_query_file_path = f'{os.path.dirname(os.path.realpath(__file__))}/test_data/just_one_match_co-located_query_file.fas'
        self.co_located_result_output_file_path = f'{os.path.dirname(os.path.realpath(__file__))}/test_data/co-located_result_output.tsv'
        self.not_co_located_result_output_file_path = f'{os.path.dirname(os.path.realpath(__file__))}/test_data/not_co-located_result_output.tsv'
        self.just_one_match_result_output_file_path = f'{os.path.dirname(os.path.realpath(__file__))}/test_data/just_one_match_result_output.tsv'
        self.genome_file_list_result_output_file_path = f'{os.path.dirname(os.path.realpath(__file__))}/test_data/genome_file_list_result_output.tsv'


        self.co_located_result_dict = [
            {
                'percent_id': 97.3,
                'query': 'seq1',
                'subject': 'NODE_1_length_200_cov_15',
                'subject_end': 104,
                'subject_start': 30
            },
            {
                'percent_id': 100.0,
                'query': 'seq2',
                'subject': 'NODE_1_length_200_cov_15',
                'subject_end': 175,
                'subject_start': 126
            }
        ]
        self.just_one_match_result = [
            {
                'percent_id': 97.3,
                'query': 'seq1',
                'subject': 'NODE_1_length_200_cov_15',
                'subject_end': 104,
                'subject_start': 30
            },
            {
                'percent_id': 'NA',
                'query': 'seq2',
                'subject': 'NA',
                'subject_end': 'NA',
                'subject_start': 'NA'
            }
        ]
        self.non_co_located_result = [
            {
                'percent_id': 97.3,
                'query': 'seq1',
                'subject': 'NODE_1_length_200_cov_15',
                'subject_end': 81,
                'subject_start': 7
            },
            {
                'percent_id': 96.2,
                'query': 'seq2',
                'subject': 'NODE_2_length_75_cov_20',
                'subject_end': 104,
                'subject_start': 1
            }
        ]


    
    def test_make_blast_database(self):
        with tempfile.TemporaryDirectory() as tmpdirname:
            make_blast_database(self.genome_file_path, f'{tmpdirname}/blast_database')
            self.assertTrue(os.path.exists(f'{tmpdirname}/blast_database.nhr'))
            
    def test_blast_search(self):
        with tempfile.TemporaryDirectory() as tmpdirname:
            make_blast_database(self.genome_file_path, f'{tmpdirname}/blast_database')
            
            # test co_located
            blast_search(self.co_located_query_file_path, f'{tmpdirname}/blast_database', tmpdirname)
            results = parse_blast(f'{tmpdirname}/blast_result.xml')
            self.assertEqual(self.co_located_result_dict, results)
            
             # test just one match
            blast_search(self.just_one_match_co_located_query_file_path, f'{tmpdirname}/blast_database', tmpdirname)
            results = parse_blast(f'{tmpdirname}/blast_result.xml')
            self.assertEqual(self.just_one_match_result, results)
            
             # test non co_located
            blast_search(self.non_co_located_query_file_path, f'{tmpdirname}/blast_database', tmpdirname)
            results = parse_blast(f'{tmpdirname}/blast_result.xml')
            self.assertEqual(self.non_co_located_result, results)

    # method to return output from writing blast result to a file
    def blast_search_file_content(self, query_filepath):
        with tempfile.NamedTemporaryFile(mode = "w+") as result_output, tempfile.TemporaryDirectory() as tmpdirname:
            make_blast_database(self.genome_file_path, f'{tmpdirname}/blast_database')
            
            # test co_located
            blast_search(query_filepath, f'{tmpdirname}/blast_database', tmpdirname)
            results = parse_blast(f'{tmpdirname}/blast_result.xml')
            genome_prefix = os.path.splitext(os.path.basename(self.genome_file_path))[0]
            write_result_table({genome_prefix: results}, result_output.name)
            result_output.seek(0)

            output_string = result_output.read()
        return output_string

    def test_output_file_creation(self):
        # test co-located
        output_string = self.blast_search_file_content(self.co_located_query_file_path)
        with open(self.co_located_result_output_file_path) as test_result_output_file:
            expected_output_string = test_result_output_file.read()
        assert expected_output_string == output_string
        
        # test not co-located
        output_string = self.blast_search_file_content(self.non_co_located_query_file_path)
        with open(self.not_co_located_result_output_file_path) as test_result_output_file:
            expected_output_string = test_result_output_file.read()
        assert expected_output_string == output_string

        # test just one match
        output_string = self.blast_search_file_content(self.just_one_match_co_located_query_file_path)
        with open(self.just_one_match_result_output_file_path) as test_result_output_file:
            expected_output_string = test_result_output_file.read()
        assert expected_output_string == output_string

    def test_find_co_located(self):
        # test genome_file
        with tempfile.NamedTemporaryFile(mode = "w+") as result_output:
            find_co_located(self.co_located_query_file_path, result_output.name, 0.95, genome_file=self.genome_file_path)
            result_output.seek(0)
            output_string = result_output.read()
            with open(self.co_located_result_output_file_path) as test_result_output_file:
                expected_output_string = test_result_output_file.read()
            assert expected_output_string == output_string
        
        # test genome_file_list
        with tempfile.NamedTemporaryFile(mode = "w+") as genome_file_list, tempfile.NamedTemporaryFile(mode = "w+") as result_output:

            genome_file_list.write(f'{self.genome_file_path}\n')
            genome_file_list.write(f'{self.genome_file_path2}\n')
            genome_file_list.seek(0)

            find_co_located(self.co_located_query_file_path, result_output.name, 0.95, genome_file_list=genome_file_list.name)
            result_output.seek(0)
            output_string = result_output.read()

            with open(self.genome_file_list_result_output_file_path) as test_result_output_file:
                expected_output_string = test_result_output_file.read()
            assert expected_output_string == output_string


            
            
            
        
        